
import './App.css';
import Navbar from './components/Navbar';
import {BrowserRouter as Router,Switch,Link,Route} from 'react-router-dom';
import PostList from './components/PostList';

function App() {
  return (
    <div className="App">
     <Router>
       
       {/* <Link to={'/'} > home</Link>
      <Link to={'/passengers'} > Passengers</Link> */}

      <Switch>
          <Route path='/passengers' ><PostList /></Route>
          <Route path='/' ><Navbar/></Route>
          
          
      </Switch>

    </Router>
    </div>
  );
}

export default App;
