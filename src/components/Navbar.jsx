import React,{useState,useEffect} from 'react'
import './navb.scss'
import {BrowserRouter as Router,Switch,Link,Route} from 'react-router-dom';
import {Menu,Close} from '@material-ui/icons';
import Modal from './Modal';
import PostList from './PostList';


function Navbar() {
    const [modalIsOpen,setModalIsOpen]=useState(false);
    const [min,setMin]=useState(10);
    const [sec,setSec]=useState(0);
    const [stop,setStop]=useState(false);

    const [openPosts,setOpenPosts]=useState(false);
    const [menu,setMenu]=useState(false);

    useEffect(() => {

        // if(updateM === 1 && updateS === 58) return;
        if(stop || (min===0 && sec===0)) return;
        const intervalId =setInterval(run,1000);
        
        // setInterv(setInterval(run,1000));
        
            return ()=>{ clearInterval(intervalId)};

    },[min,sec])
    
    const run = () => {
        setSec((sec)=> sec -1);
        
        if(sec ===  0 )
        { 
            setMin((min)=> min -1);
            setSec(59);
        }

    }

    

    const handleModal = () =>{
        setModalIsOpen(true);
    }
    return (
        <div className="content">
        <div className="navbar">
            <div className="navbarWrapper">
                <div className="navbarLeft">
                     <div className="navbarImg">
                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTut9whWF9Hb1NAvk5Z-c3hQz6SS8n2QgtOWA&usqp=CAU" alt="" className="image"/>
                    </div>
                    <div className="navbarText-web">
                        Trail Lesson Grade[1-3]
                    </div>
                    <div className="navbarText-mweb">
                        <h2>Codingal</h2>
                    </div>
                    <div className="passengers"><Link to={'/passengers'} > Passengers</Link></div>
                    
                    
                    
                </div>
                <div className={`navbarRight ${menu ? "active" : ""}`}>
                    <div className={`hamburger`} onClick={()=> setMenu(!menu)}>
                        {menu ? <Close /> : <Menu />  }
                    </div>
                    
                        <div className={`hamcontainer ${menu ? "active" : ""}`}>

                            <div className="navbarTimer">
                            {min=== 10 ? min : "0"+min}:{sec>=10 ? sec : "0"+sec }
                            </div>
                            <div className="navbarTask">
                            <button onClick={handleModal} className="btn"> End Class</button>
                            </div>

                        </div>        
                    
                    
                    
                   
                </div>
            </div>
        </div>
        
        {/* {
            openPosts && <Router>
                <Switch>
                    <Route path="/passengers"><PostList /></Route>
                </Switch>
            </Router>
        } */}
        
       
         {modalIsOpen && <Modal setModalIsOpen={setModalIsOpen} setStop={setStop}/>}
    </div>
    )
}

export default Navbar
