import React,{useState,useEffect} from 'react'
import InfiniteScroll from 'react-infinite-scroll-component';
import axios from 'axios';
import {BeatLoader} from 'react-spinners';
import './postlist.scss';


function PostList() {

    const [posts,setPosts]=useState([]);
    const [page,setPage] = useState(0);
    const [isLoading,setIsLoading] = useState(true);

    useEffect(() => {

        setIsLoading(true);
        axios.get(`https://api.instantwebtools.net/v1/passenger?page=${page}&size=10`)
            .then((res) =>{
                console.log(res);
                // const 
                console.log(posts);
                setPosts([...posts, ...res.data.data]);
                console.log(posts);
                setIsLoading(false);

                
            })
            .catch((error) =>{
                console.log(error);
            })


            
    },[page])

    return (
        <div className="post">

            <InfiniteScroll 
                dataLength={posts.length}
                next={()=>{setPage(page+1)}}
                hasMore={true}
                
            >  
            <div className="postheading">
                <div><h1>Name</h1></div>
                <div><h1>#Trips</h1></div> 
            </div>
                                         
            {
                posts.length ? posts.map((post) => { return <div className="post_container" key={post.id}>
                    
                    <div><h2>{post.name}</h2></div>
                    <div ><h2>{post.trips}</h2></div>
                </div>
                }) : " "
            }
            </InfiniteScroll>
            {
                isLoading && <div className="loader"><BeatLoader size={24} color='gray' loading={isLoading}/></div>
            } 
        </div>
    )
}

export default PostList
