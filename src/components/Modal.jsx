import React,{useState} from 'react'
import './modal.scss';
import {Close} from '@material-ui/icons';
import {Radio} from '@material-ui/core';


function Modal({setModalIsOpen,setStop}) {

    const [check,setCheck] = useState("");
    const [subcheck,setSubCheck]=useState("");
    const [text,setText] = useState("");

    const handleRadio =(e)=>{
        setCheck(e.target.value);
        console.log(e.target.value);
        setSubCheck("");
    }

    const handleRadioGrp = (e)=>{
        setSubCheck(e.target.value);

    }
    const handletext = (e)=>{
        setText(e.target.value);
        console.log(e.target.value);
    }
    return (
        <div className="modalBackground">

             <div className="modalContainer">
                    <div className="modalclose"  onClick={()=>{setModalIsOpen(false)}}>
                                <Close />
                    </div>
                    <div className="modaltext">
                            <h3>Select a reason to end class</h3>
                    </div>
                    
                    <div className="completed">
                             <Radio value="complete" checked={check==='complete'} onChange={handleRadio} />
                                 <span>Class completed</span>
                    </div>
                                
                                <div className="interupt">
                                    <Radio value="interupt" checked={check==='interupt'} onChange={handleRadio} />
                                    <span>Class interrupted/aborted</span>

                                    {
                                        (check==='interupt') && <div className="listContainer">
                                            <div>
                                                <Radio value="donotshowup" checked={subcheck==='donotshowup'} onChange={handleRadioGrp} />
                                                <span>Student didn't show up for the class.</span>
                                            </div>
                                            <div>
                                                <Radio value="donotshowany" checked={subcheck==='donotshowany'} onChange={handleRadioGrp} />
                                                <span>Student didn't show any interest.</span>
                                            </div>
                                            <div>
                                                <Radio value="studis" checked={subcheck==='studis'} onChange={handleRadioGrp} />
                                                <span>Student got disconnected.</span>
                                            </div>
                                            <div>
                                                <Radio value="disconnect" checked={subcheck==='disconnect'} onChange={handleRadioGrp} />
                                                <span>I got disconnected.</span>
                                            </div>
                                            <div >
                                                <Radio value="other" checked={subcheck==='other'} onChange={handleRadioGrp} />
                                                <span>Other reason.</span>
                                                {
                                                    subcheck==='other' && <form>
                                                        <textarea name="reason" placeholder="Type here..." className="other" onChange={handletext}></textarea>
                                                    </form>
                                                }
                                            </div>

                                        </div>
                                    }
                                </div>
                                <div className="btncontainer">
                                    <button onClick={()=>{

                                        if(check==='complete'){
                                            setModalIsOpen(false);
                                            setStop(true);
                                        }
                                        if(check==='interupt' && (subcheck==='donotshowup' || subcheck==='donotshowany' || subcheck==='studis' || subcheck==='disconnect' || (subcheck==='other' && text.trim()!=="" ) )){
                                            setModalIsOpen(false);
                                            setStop(true);
                                        }
                                        }} className="btn"> End Class</button>
                                    <button onClick={()=>{setModalIsOpen(false)}} className="btn">Cancel</button>
                                </div>        
                                
                </div>                            

            
        </div>
    )
}

export default Modal
